package br.com.fiap.bo;

import java.util.ArrayList;
import java.util.List;

import br.com.fiap.beans.Cliente;
import br.com.fiap.dao.ClienteDAO;

public class ClienteBO {
	public static String novoCliente(Cliente obj)throws Exception{
		if (obj.getQtdeEstrelas()<1 || obj.getQtdeEstrelas()>5){
			return "Esqueci a RN de qtdeEstrelas";
			//throw new RuntimeException();
		}
		if (obj.getNome().length()>40){
			return "Estourou o tamanho do Nome";
		}
		if (obj.getNumero()<1){
			return "Numero zero ou negativo";
		}
		ClienteDAO dao = new ClienteDAO();
		Cliente resp = dao.getCliente(obj.getNumero());
		if (resp.getNumero()>0){
			dao.fechar();
			return "Cliente j� existe!";
		}
		String x = dao.gravar(obj);
		dao.fechar();
		return x;
	}
	
	public static Cliente consultarPorNumero(int pNumero)throws Exception{
		if(pNumero<1){
			return new Cliente();
		}
		ClienteDAO dao = new ClienteDAO();
		Cliente resp = dao.getCliente(pNumero);
		dao.fechar();
		return resp;
	}
	
	public static String aumentarQtdeEstrelas(int pNumeroCliente) throws Exception{
		ClienteDAO dao = new ClienteDAO();
		if(pNumeroCliente<1){
			return "Numero de cliente invalio";
		}
		Cliente y = dao.getCliente(pNumeroCliente);
		if(y.getQtdeEstrelas() > 4){
			dao.fechar();
			return "M�ximo de estrelas alca�adas";			
		}
		String x = dao.uparNivel(pNumeroCliente);
		dao.fechar();
		return x;
	}
	
	public static String excluir(int pNumeros) throws Exception{
		ClienteDAO dao = new ClienteDAO();
		if(pNumeros<1){
		dao.fechar();
		return "Numero invalido";
		}				
		String x = dao.excluir(pNumeros);
		dao.fechar();
		return x;
	}
	
	public static List<Cliente> listarPorNome (String listNome) throws Exception{
		if(listNome.length()>50){
			return new ArrayList<Cliente>();
		}
		ClienteDAO dao = new ClienteDAO();		
		List<Cliente> x  = dao.listarPorNome(listNome);
		dao.fechar();		
		return x;
	}
}







