package br.com.fiap.bo;

import br.com.fiap.beans.Produto;
import br.com.fiap.dao.ProdutoDAO;

public class ProdutoBO {
	public String novoProd(Produto obj) throws Exception{
		
		if(obj.getCodigo()<1){
			return "Numero zero ou invalido";
		}
		
		if(obj.getDescricao().length() >10 && obj.getDescricao().length()<50){
			return "Estorou o tamanho do nome";
		}
		
		if(obj.getValor()<10){
			return "valor tem que ser acima de 10";
		}
		
		if((obj.getCategoria() != "Eletronicos") || (obj.getCategoria() != "Acessorios") || (obj.getCategoria() != "Eletroeletronicos")){
			return "Categoria Invalida";
		}
		ProdutoDAO dao = new ProdutoDAO();
		
		Produto resp = dao.consultarPorCodigo(obj.getCodigo());
		if(resp.getCodigo()>0){
			dao.fechar();
			return "Cliente ja existe";
		}
		
		
		
		String x = dao.gravar(obj);
		dao.fechar();
		return x + "Gravado";	
		
	}

}
