package br.com.fiap.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.com.fiap.beans.Cliente;
import br.com.fiap.conexao.ConexaoFactory;

public class ClienteDAO {
	private Connection con;

	public ClienteDAO()throws Exception{
		con = new ConexaoFactory().connect();
	}
	public String fechar() throws Exception{
		con.close();
		return "Parab�ns Juninho!";
	}
	
	public String gravar(Cliente cli) throws Exception{
		PreparedStatement estrutura = 
				con.prepareStatement("INSERT INTO TB_POO_CLIENTE "
				+ "(NM_CLIENTE, NR_CLIENTE, QT_ESTRELAS) VALUES (?,?,?)");
		estrutura.setString(1, cli.getNome());
		estrutura.setInt(2, cli.getNumero());
		estrutura.setInt(3, cli.getQtdeEstrelas());
		int churros = estrutura.executeUpdate();
		estrutura.close();
		return churros + " cliente(s) foi(ram) adicionado(s)";
	}

	public String uparNivel(int pNumero)throws Exception{
		PreparedStatement estrutura = con.prepareStatement
	("UPDATE TB_POO_CLIENTE SET QT_ESTRELAS=QT_ESTRELAS+1 "
			+ " WHERE NR_CLIENTE=?");
		estrutura.setInt(1, pNumero);
		int x = estrutura.executeUpdate();
		estrutura.close();
		return x + " cliente(s) foi(ram) updado(s)!";
	}
	public String excluir(int pNumero) throws Exception{
		PreparedStatement estrutura = con.prepareStatement("DELETE FROM TB_POO_CLIENTE WHERE NR_CLIENTE=?");
		estrutura.setInt(1, pNumero);
		int x = estrutura.executeUpdate();
		estrutura.close();
		return x + "Foram alteradas";
	}
	public Cliente getCliente(int pNumero)throws Exception{
		PreparedStatement estrutura = con.prepareStatement("SELECT * FROM TB_POO_CLIENTE WHERE NR_CLIENTE=?");
		estrutura.setInt(1, pNumero);
		ResultSet resultado = estrutura.executeQuery();
		Cliente obj = new Cliente();
		if(resultado.next()){
			obj.setNome(resultado.getString("NM_CLIENTE"));
			obj.setNumero(resultado.getInt("NR_CLIENTE"));
			obj.setQtdeEstrelas(resultado.getInt("QT_ESTRELAS"));
		}
		resultado.close();
		estrutura.close();
		return obj;
	}
	public List<Cliente> listarPorNome
		(String pNome)throws Exception{
		PreparedStatement stmt = 
				con.prepareStatement
				("SELECT * FROM TB_POO_CLIENTE WHERE NM_CLIENTE LIKE ?");
		stmt.setString(1, pNome + "%");
		ResultSet rs = stmt.executeQuery();
		List<Cliente> lista = new ArrayList<Cliente>();
		Cliente obj = null;
		while(rs.next()){
			obj=new Cliente();
			obj.setNome(rs.getString("NM_CLIENTE"));
			obj.setNumero(rs.getInt("NR_CLIENTE"));
			obj.setQtdeEstrelas(rs.getInt("QT_ESTRELAS"));
			lista.add(obj);
		}
		rs.close();
		stmt.close();
		return lista;
		
		
	}
}









