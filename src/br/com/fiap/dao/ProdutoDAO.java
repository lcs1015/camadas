package br.com.fiap.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import br.com.fiap.beans.Cliente;
import br.com.fiap.beans.Produto;
import br.com.fiap.conexao.ConexaoFactory;

public class ProdutoDAO {
	private Connection con;

	public ProdutoDAO()throws Exception{
		con = new ConexaoFactory().connect();
	}
	public void fechar() throws Exception{
		con.close();
	}
	
	public String gravar(Produto obj) throws Exception{
		PreparedStatement estrutura = 
				con.prepareStatement("INSERT INTO TB_POO_PRODUTO "
				+ "(CD_PRODUTO, DS_PRODUTO, NM_CATEGORIA,VL_PRODUTO) VALUES (?,?,?,?)");
		estrutura.setInt(1, obj.getCodigo());
		estrutura.setString(2, obj.getDescricao());
		estrutura.setString(3, obj.getCategoria());
		estrutura.setDouble(4, obj.getValor());
		estrutura.executeUpdate();
		estrutura.close();
		return "Gravado com sucesso!";
	}
	public Produto consultarPorCodigo(int pNumero)throws Exception{
		PreparedStatement estrutura = con.prepareStatement
				("SELECT * FROM TB_POO_PRODUTO WHERE CD_PRODUTO=?");
		estrutura.setInt(1, pNumero);
		ResultSet resultado = estrutura.executeQuery();
		Produto obj = new Produto();
		if(resultado.next()){
			obj.setDescricao(resultado.getString("DS_PRODUTO"));
			obj.setCodigo(resultado.getInt("CD_PRODUTO"));
			obj.setCategoria(resultado.getString("NM_CATEGORIA"));
			obj.setValor(resultado.getDouble("VL_PRODUTO"));
		}
		resultado.close();
		estrutura.close();
		return obj;
	}

	public Produto consultarPorValor(double pValor)throws Exception{
		PreparedStatement estrutura = con.prepareStatement
				("SELECT * FROM TB_POO_PRODUTO WHERE VL_PRODUTO>?");
		estrutura.setDouble(1, pValor);
		ResultSet resultado = estrutura.executeQuery();
		Produto obj = new Produto();
		if(resultado.next()){
			obj.setDescricao(resultado.getString("DS_PRODUTO"));
			obj.setCodigo(resultado.getInt("CD_PRODUTO"));
			obj.setCategoria(resultado.getString("NM_CATEGORIA"));
			obj.setValor(resultado.getDouble("VL_PRODUTO"));
		}
		resultado.close();
		estrutura.close();
		return obj;
	}
	
	public String aumentarSal(float valor) throws Exception{
		if(valor <0 || valor > 10){
			return "Valor invalido";
		}
		PreparedStatement estrutura = con.prepareStatement(
				"UPDATE TB_POO_PRODUTO SET VL_PRODUTO=VL_PRODUTO*?");		
		estrutura.setDouble(1, (valor/100)+1);
		int x = estrutura.executeUpdate();
		estrutura.close();
		
		return x + "O pre�o foi alterado";
				
	}

}









