package br.com.fiap.principal;

import javax.swing.JOptionPane;

import br.com.fiap.beans.Cliente;
import br.com.fiap.dao.ClienteDAO;

public class TesteGravarCliente {

	public static void main(String[] args) {
		ClienteDAO dao = null;
		try{
			dao = new ClienteDAO();
			//Cliente obj = new Cliente();
			//obj.setNome(JOptionPane.showInputDialog("Nome"));
			//obj.setNumero(Integer.parseInt(JOptionPane.showInputDialog("Numero")));
			//obj.setQtdeEstrelas(Integer.parseInt(JOptionPane.showInputDialog("Estrelas")));
			//dao.gravar(obj);
			System.out.println(dao.gravar(new Cliente
					(JOptionPane.showInputDialog
					("Digite o nome"), 
					Integer.parseInt
					(JOptionPane.showInputDialog("Numero")), 
					Integer.parseInt
					(JOptionPane.showInputDialog("QtdeEstrelas")))));
		}catch (Exception e){
			e.printStackTrace();
		}finally{
			try{
				dao.fechar();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
}
