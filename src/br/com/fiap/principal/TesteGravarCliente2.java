package br.com.fiap.principal;

import javax.swing.JOptionPane;

import br.com.fiap.beans.Cliente;
import br.com.fiap.bo.ClienteBO;
import br.com.fiap.excecao.Exececao;

public class TesteGravarCliente2 {

	public static void main(String[] args) {
		try{
			System.out.println(ClienteBO.novoCliente(new Cliente
					(JOptionPane.showInputDialog
					("Digite o nome"), 
					Integer.parseInt
					(JOptionPane.showInputDialog("Numero")), 
					Integer.parseInt
					(JOptionPane.showInputDialog("QtdeEstrelas")))));
		}catch (Exception e){
			System.out.println(Exececao.tratarErro(e));
		}
	}
}
