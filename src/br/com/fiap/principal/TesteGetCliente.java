package br.com.fiap.principal;

import javax.swing.JOptionPane;

import br.com.fiap.beans.Cliente;
import br.com.fiap.dao.ClienteDAO;

public class TesteGetCliente {

	public static void main(String[] args) {
		ClienteDAO dao = null;
		try{
			dao = new ClienteDAO();
			Cliente obj = dao.getCliente
					(Integer.parseInt
					(JOptionPane.showInputDialog
					("Digite o n�mero")));
			System.out.println("Nome: " + obj.getNome());
			System.out.println("N�mero: " + obj.getNumero());
			System.out.println("Estrelas: "+ obj.getQtdeEstrelas());
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				dao.fechar();
			}catch(Exception e){
				e.printStackTrace();
			}
		}

	}

}
