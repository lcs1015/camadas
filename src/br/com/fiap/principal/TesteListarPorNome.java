package br.com.fiap.principal;

import java.util.List;

import javax.swing.JOptionPane;

import br.com.fiap.beans.Cliente;
import br.com.fiap.dao.ClienteDAO;

public class TesteListarPorNome {

	public static void main(String[] args) {
		ClienteDAO dao = null;
		try{
			dao= new ClienteDAO();
			List<Cliente> lista = 
					dao.listarPorNome
					(JOptionPane.showInputDialog
					("Digite o nome").toUpperCase());
			for(Cliente c : lista){
				System.out.println("Nome: " + c.getNome());
				System.out.println("Numero: " + c.getNumero());
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				dao.fechar();
			}catch(Exception e){
				e.printStackTrace();
			}
		}

	}

}
