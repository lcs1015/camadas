package br.com.fiap.principal;

import javax.swing.JOptionPane;

import br.com.fiap.beans.Cliente;
import br.com.fiap.bo.ClienteBO;

public class TesteGetCliente2 {

	public static void main(String[] args) {
		try{
			Cliente obj = ClienteBO.consultarPorNumero
					(Integer.parseInt
					(JOptionPane.showInputDialog
					("Digite o n�mero")));
			System.out.println("Nome: " + obj.getNome());
			System.out.println("N�mero: " + obj.getNumero());
			System.out.println("Estrelas: "+ obj.getQtdeEstrelas());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
