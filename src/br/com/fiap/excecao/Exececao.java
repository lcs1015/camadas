package br.com.fiap.excecao;

import java.sql.SQLException;

public class Exececao extends Exception{
	public static String tratarErro(Exception e){
		if(e instanceof SQLException){
			return "Login negado";
		}else if (e instanceof NumberFormatException){
			return "Numero Invalido";
		}else{
			return "Erro Desconhecido";
		}
		
	}

}
