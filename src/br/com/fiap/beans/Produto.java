package br.com.fiap.beans;

public class Produto {
	private String descricao;
	private String categoria;
	private double valor;
	private int codigo;
	public Produto(String descricao, String categoria, double valor, int codigo) {
		super();
		this.descricao = descricao;
		this.categoria = categoria;
		this.valor = valor;
		this.codigo = codigo;
	}
	public Produto() {
		super();
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao.toUpperCase();
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria.toUpperCase();
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

}
