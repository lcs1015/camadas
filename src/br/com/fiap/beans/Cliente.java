package br.com.fiap.beans;

public class Cliente {
	private String nome;
	private int numero;
	private int qtdeEstrelas;
	public Cliente(String nome, int numero, int qtdeEstrelas) {
		setNome(nome);
		setNumero(numero);
		setQtdeEstrelas(qtdeEstrelas);
	}
	public Cliente() {
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome.toUpperCase();
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public int getQtdeEstrelas() {
		return qtdeEstrelas;
	}
	public void setQtdeEstrelas(int qtdeEstrelas) {
		this.qtdeEstrelas = qtdeEstrelas;
	}

	
}
